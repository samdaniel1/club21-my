SELECT lineitem_name , product_type as category, vendor as brand, lineitem_price as price , lineitem_discount as discount, lineitem_quantity as quantity , DATE(c.created_at) as order_date, c.customer__id, IFNULL(SPLIT(lineitem_variant_title, '/')[SAFE_OFFSET(0)], null) as size , IFNULL(SPLIT(lineitem_variant_title, '/')[SAFE_OFFSET(1)], null) as colour, SPLIT(lineitem_variant_title, '/')[SAFE_OFFSET(2)] as third_preference, f.email, f.phone,CASE WHEN json_extract_scalar(h, '$.code') IS NULL THEN 'Not Applied'ELSE json_extract_scalar(h, '$.code') END AS order_level_code,

CASE WHEN
lineitem_discount = 0 THEN 0
ELSE ROUND(lineitem_discount/lineitem_price*100, 2) END AS lineitem_discount_percentage,

CASE WHEN
lineitem_discount = 0 THEN 'No Discount'
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) < 11 THEN '<= 10 percent'
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) BETWEEN 11 and 20 THEN '11-20 percent'
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) BETWEEN 21 and 30 THEN '21-30 percent' 
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) BETWEEN 31 and 40 THEN '31-40 percent' 
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) BETWEEN 41 and 50 THEN '41-50 percent' 
WHEN ROUND(lineitem_discount/lineitem_price *100, 2) >=50 THEN '>= 50 percent' 
END AS lineitem_discount_group,

sensitivity_group, recency_group, frequency_group, monetary_group, join_date last_purchase_date, duration, recency, frequency, total_sales, abv, 
return_status, interval2, active_status, abc_class, d.billing_address__city as city, d.billing_address__province as state

FROM `pt-client-project.Club21.Order_Line_Items` as a
Left join `pt-client-project.Club21.ShopifyMY_Products_Data` as b
on a.lineitem_name = b.title
--on a.lineitem_product_id = CAST(b.id AS STRING)
Left join `pt-client-project.Club21.Orders` as c
on a.id = CAST (c.id AS STRING)
LEFT JOIN `pt-client-project.Club21.Customers_Location` as d
on c.customer__id = d.customer__id
RIGHT JOIN `pt-client-project.Club21.RFM Analysis on Loyal Customer 2` as e 
ON c.customer__id = e.customer__id 
LEFT JOIN `pt-client-project.Club21.ShopifyMY_Customer_Data` as f
on c.customer__id = f.id
LEFT JOIN unnest(json_extract_array(discount_codes)) AS h


where DATE(c.created_at) >= '2021-05-22' and c.customer__id IS NOT NULL
Order by 5 ASC
