with max_date as (select max(DATE(created_at)) from `pt-client-project.Club21.Orders`
#where DATE(created_at) between @DS_START_DATE and @DS_END_DATE )
)
select customer__id, 
case when 
sum(total_discounts_set__shop_money__amount) = 0 THEN 'insensitive'
when round(sum(total_discounts_set__shop_money__amount)/(sum(total_price)+sum(total_discounts_set__shop_money__amount))*100, 2) > 20 then 'sensitive' 
when round(sum(total_discounts_set__shop_money__amount)/(sum(total_price)+sum(total_discounts_set__shop_money__amount))*100, 2) > 5 then 'moderate sensitivity' 
else 'insensitive' end as sensitivity_group,
case when ntile(3) over (order by max(DATE(created_at))) = 1 then '1 At Risk' 
when ntile(3) over (order by max(DATE(created_at))) = 2 then '2 Slipping Away'
when ntile(3) over (order by max(DATE(created_at))) = 3 then '3 Active' end as recency_group,

case when ntile(3) over (order by count(distinct id)) = 1 then '1 Low Frequency' 
when ntile(3) over (order by count(distinct id)) = 2 then '2 Moderate Frequency'
when ntile(3) over (order by count(distinct id)) = 3 then '3 High Frequency' end as frequency_group,

case when sum(total_price) = 0 then '0 No Spender'
when ntile(3) over (order by sum(total_price)/count(distinct id)) = 1 then '1 Low Spender' 
when ntile(3) over (order by sum(total_price)/count(distinct id)) = 2 then '2 Moderate Spender'
when ntile(3) over (order by sum(total_price)/count(distinct id)) = 3 then '3 High Spender' end as monetary_group,

--round((sum(total_discounts_set__shop_money__amount)/sum(total_price)), 4) as discount_level,
case when sum(total_discounts_set__shop_money__amount) = 0 Then 0
else round(sum(total_discounts_set__shop_money__amount)/(sum(total_price)+sum(total_discounts_set__shop_money__amount)), 4) end as discount_level,
min(DATE(created_at)) as join_date,
max(DATE(created_at)) as last_purchase_date,
date_diff(max(DATE(created_at)), min(DATE(created_at)), day) as duration,
date_diff((select * from max_date), max(DATE(created_at)), day) as recency,
count(distinct id) as frequency,
sum(total_price) as total_sales,
sum(total_price	)/count(distinct id) as abv,
case when count(distinct id) = 1 and min(DATE(created_at)) between date_sub((select * from max_date) , interval 1 month) and (select * from max_date) then 'New' 
when count(distinct id) = 1 and min(DATE(created_at)) < date_sub((select * from max_date) , interval 1 month) then 'One-Time'
when count(distinct id) > 1 and ARRAY_AGG(DATE(created_at) ORDER BY DATE(created_at) asc LIMIT 2)[SAFE_ORDINAL(2)] between date_add(min(DATE(created_at)), interval 0 day) and date_add(min(DATE(created_at)), interval 1 month) then 'Active Return'
when count(distinct id) > 1 and ARRAY_AGG(DATE(created_at) ORDER BY DATE(created_at) asc LIMIT 2)[SAFE_ORDINAL(2)] between date_add(min(DATE(created_at)), interval 0 day) and date_add(min(DATE(created_at)), interval 3 month) then 'Return Within 3 months' 
when count(distinct id) > 1 and ARRAY_AGG(DATE(created_at) ORDER BY DATE(created_at) asc LIMIT 2)[SAFE_ORDINAL(2)] between date_add(min(DATE(created_at)), interval 0 day) and date_add(min(DATE(created_at)), interval 6 month) then 'Return Within 6 months'
when count(distinct id) > 1 then 'Non-Active Return' else null end as return_status,
DATE_DIFF(ARRAY_AGG(DATE(created_at) ORDER BY DATE(created_at) asc)[SAFE_ORDINAL(2)] , ARRAY_AGG(DATE(created_at) ORDER BY DATE(created_at) asc)[SAFE_ORDINAL(1)], DAY) as interval2,
case when max(DATE(created_at)) between date_sub((select * from max_date), interval 3 week) and (select * from max_date) then 'Active' else 'Non-Active' end as active_status,
max(class) as abc_class
from `pt-client-project.Club21.Orders` as x
left join (select cid, class from `pt-client-project.Club21.ABC Customers` group by 1,2) as y 
on x.customer__id = y.cid
where  DATE(created_at) >= '2021-05-22' 
#Where DATE(created_at) between @DS_START_DATE and @DS_END_DATE 
group by 1
