SELECT a.id as order_id, created_at, a.customer__id, SUM(lineitem_price) as ori_price, SUM(lineitem_discount) as total_plevel_dis, 
CASE WHEN SUM(lineitem_discount) = 0 THEN 0.0
ELSE ROUND(SUM(lineitem_discount)/SUM(lineitem_price)*100, 2) END AS plevel_percentage,
(SUM(lineitem_price) - SUM(lineitem_discount)) as order_level_price, CASE WHEN json_extract_scalar(h, '$.code') IS NULL THEN 'Not Applied'ELSE json_extract_scalar(h, '$.code') END AS order_level_code, 
CASE WHEN total_discounts_set__shop_money__amount = 0 THEN 0.0
ELSE ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) END AS olevel_percentage,
total_discounts_set__shop_money__amount as olevel_dis,  total_price as final_price,   
sensitivity_group, recency_group, frequency_group, monetary_group, join_date, last_purchase_date, duration, recency, frequency, total_sales, abv, 
return_status, interval2, active_status, abc_class

FROM `pt-client-project.Club21.Orders` as a
LEFT JOIN unnest(json_extract_array(discount_codes)) AS h
LEFT JOIN `pt-client-project.Club21.Order_Line_Items` as b ON CAST(a.id as STRING) = b.id
LEFT JOIN `pt-client-project.Club21.RFM Analysis on Loyal Customer 2` as c ON a.customer__id = c.customer__id 
GROUP BY 1,2,3,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26