SELECT id as order_id, a.customer__id,  created_at, total_price as final_price, total_discounts_set__shop_money__amount as discounted_price,  
CASE WHEN json_extract_scalar(h, '$.code') IS NULL THEN 'Not Applied'ELSE json_extract_scalar(h, '$.code') END AS order_level_code,  
json_extract_scalar(h, '$.amount') as order_level_discount_amount,

CASE WHEN
total_discounts_set__shop_money__amount = 0 THEN 0
ELSE ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) END AS discount_percentage,

CASE WHEN
SUM(total_discounts_set__shop_money__amount) = 0 THEN 'No Discount'
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) < 11 THEN '<= 10 percent'
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) BETWEEN 11 and 20 THEN '11-20 percent'
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) BETWEEN 21 and 30 THEN '21-30 percent' 
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) BETWEEN 31 and 40 THEN '31-40 percent' 
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) BETWEEN 41 and 50 THEN '41-50 percent' 
WHEN ROUND(total_discounts_set__shop_money__amount/(total_price + total_discounts_set__shop_money__amount)*100, 2) >=50 THEN '>= 50 percent' 
END AS discount_group,

sensitivity_group, recency_group, frequency_group, monetary_group, join_date, last_purchase_date, duration, recency, frequency, total_sales, abv, 
return_status, interval2, active_status, abc_class,

FROM `pt-client-project.Club21.Orders` as a
LEFT JOIN `pt-client-project.Club21.RFM Analysis on Loyal Customer 2` as b ON a.customer__id = b.customer__id 
LEFT JOIN unnest(json_extract_array(discount_codes)) AS h
GROUP BY 1,2,3,4,5,6,7,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24