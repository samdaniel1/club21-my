with repurchase_data as 
(select customer__id as cid, c.product_type as category_name, 
DATE_DIFF(ARRAY_AGG(distinct DATE(a.created_at) order by DATE(a.created_at))[offset(1)], ARRAY_AGG(distinct DATE(a.created_at)
order by DATE(a.created_at))[ordinal(1)], WEEK) AS IPI
from `pt-client-project.Club21.Orders` as a
LEFT JOIN `pt-client-project.Club21.Order_Line_Items` as b
ON cast (a.id as STRING) = b.id
LEFT JOIN `pt-client-project.Club21.ShopifyMY_Products_Data` as c
on b.lineitem_product_id = CAST(c.id as STRING)
#where DATE(created_at) between @DS_START_DATE and @DS_END_DATE 
Where total_price > total_discounts_set__shop_money__amount
group by 1,2
having count(distinct DATE(a.created_at)) > 1),

product_purchaser as 
(select product_type as category_name,
count(distinct customer__id) as purchasers,
sum(lineitem_quantity) as quantity_sold,
sum(lineitem_price) as total_revenue 
from `pt-client-project.Club21.Order_Line_Items` as a
left join `pt-client-project.Club21.Orders` as b
on a.id = CAST(b.id as STRING)
LEFT JOIN `pt-client-project.Club21.ShopifyMY_Products_Data` as c
on a.lineitem_product_id = CAST(c.id as STRING)
WHERE lineitem_price > 0
and total_price > total_discounts_set__shop_money__amount
group by 1),

repurchase as
(select category_name, cast(AVG(IPI) as int64) as IPI, count(distinct cid) as repurchase_customer
from repurchase_data as a
where category_name is not null
group by 1)

select z.category_name, repurchase_customer, purchasers, IPI, quantity_sold, total_revenue
from product_purchaser as z
left join repurchase as x
on trim(x.category_name) = trim(z.category_name)
order by repurchase_customer desc
limit 50