WITH table1 AS(
  SELECT category as cat_name,  
  SUM(quantity) AS total_units_sold, 
  SUM(price) AS total_units_revenue,
  FROM `pt-client-project.Club21.Sales Report`
  GROUP BY cat_name
  ORDER BY total_units_sold DESC),

table2 AS(
  SELECT cat_name, total_units_sold, total_units_revenue, 
  SUM(total_units_sold) OVER () AS units,
  SUM(total_units_revenue) OVER () AS revenue
  FROM table1
  GROUP BY cat_name,total_units_sold,total_units_revenue
  ORDER BY units DESC),

table3 AS (
SELECT cat_name, total_units_sold, units, total_units_revenue, revenue,
(total_units_sold/units)*100 AS perc_units_sold,
(total_units_revenue/revenue)*100 AS perc_revenue,
(total_units_revenue/revenue)*100 
FROM table2
GROUP BY cat_name,total_units_sold, units, total_units_revenue, revenue
ORDER BY perc_revenue DESC),

table4 AS(
SELECT cat_name, total_units_sold, units, total_units_revenue, revenue,perc_revenue,
  perc_units_sold,
SUM(perc_revenue) OVER (ORDER BY perc_revenue DESC) AS cumulative_perc_revenue,
FROM table3
GROUP BY cat_name,total_units_sold, units, total_units_revenue, revenue, perc_revenue,
  perc_units_sold
ORDER BY perc_revenue DESC)

SELECT cat_name, total_units_sold, total_units_revenue, perc_revenue,cumulative_perc_revenue,
case when NTILE(5) over (order by cumulative_perc_revenue) IN (1) then 'A'
  when NTILE(5) over (order by cumulative_perc_revenue) IN (2,3) then 'B'
  when NTILE(5) over (order by cumulative_perc_revenue) IN (4,5) then 'C' end as class
FROM table4
GROUP BY cat_name,total_units_sold, total_units_revenue, perc_revenue,cumulative_perc_revenue
ORDER BY cumulative_perc_revenue ASC