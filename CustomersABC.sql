with customer_data as
(select customer__id as cid, sum(total_price) as total_revenue from `pt-client-project.Club21.Orders` 
--where total_price > total_discounts_set__shop_money__amount #and DATE(created_at) between @DS_START_DATE and @DS_END_DATE 
group by 1)
SELECT
	cid, 
	total_revenue as acs,
	SUM(total_revenue) OVER (ORDER BY total_revenue DESC) AS CumulativeRevenue,
	SUM(total_revenue) OVER () AS TotalRevenue,
	SUM(total_revenue) OVER (ORDER BY total_revenue DESC) / SUM(total_revenue) OVER () AS CumulativePercentage,
	CASE WHEN SUM(total_revenue) OVER (ORDER BY total_revenue DESC) / SUM(total_revenue) OVER () < 0.5 THEN 'A'
		WHEN SUM(total_revenue) OVER (ORDER BY total_revenue DESC) / SUM(total_revenue) OVER () < 0.8 THEN 'B'
		ELSE 'C'
	END AS Class
FROM customer_data
GROUP BY cid, acs
order by acs desc